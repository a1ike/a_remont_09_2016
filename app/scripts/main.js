$(document).ready(function() {

  $('.item-js').first().addClass('active');

  $('.fancybox').fancybox({
    padding: 0,
    openEffect: 'elastic',
    closeBtn: false
  });

  $('#owl-home').owlCarousel({

    navigation: false, // Show next and prev buttons
    slideSpeed: 300,
    pagination: true,
    paginationSpeed: 400,
    singleItem: true,
    autoPlay: true,
    navigationText: [
      '<img src=\'images/owl_left.png\'>',
      '<img src=\'images/owl_right.png\'>'
    ]

    // "singleItem:true" is a shortcut for:
    // items : 1, 
    // itemsDesktop : false,
    // itemsDesktopSmall : false,
    // itemsTablet: false,
    // itemsMobile : false

  });

  $('#owl-reviews').owlCarousel({

    navigation: false, // Show next and prev buttons
    pagination: false,
    slideSpeed: 300,
    paginationSpeed: 400,
    singleItem: true,
    autoPlay: true

    // "singleItem:true" is a shortcut for:
    // items : 1, 
    // itemsDesktop : false,
    // itemsDesktopSmall : false,
    // itemsTablet: false,
    // itemsMobile : false

  });

  $('#owl-stocks').owlCarousel({

    navigation: false, // Show next and prev buttons
    pagination: false,
    slideSpeed: 300,
    paginationSpeed: 400,
    singleItem: true,
    autoPlay: true

    // "singleItem:true" is a shortcut for:
    // items : 1, 
    // itemsDesktop : false,
    // itemsDesktopSmall : false,
    // itemsTablet: false,
    // itemsMobile : false

  });

  $('#owl-decoration-1').owlCarousel({

    navigation: true, // Show next and prev buttons
    slideSpeed: 300,
    paginationSpeed: 400,
    singleItem: true,
    pagination: false,
    navigation: true,
    navigationText: [
      '<img src=\'images/owl_left.png\'>',
      '<img src=\'images/owl_right.png\'>'
    ]

    // "singleItem:true" is a shortcut for:
    // items : 1, 
    // itemsDesktop : false,
    // itemsDesktopSmall : false,
    // itemsTablet: false,
    // itemsMobile : false

  });

  $('#owl-decoration-2').owlCarousel({

    navigation: true, // Show next and prev buttons
    slideSpeed: 300,
    paginationSpeed: 400,
    singleItem: true,
    pagination: false,
    navigation: true,
    navigationText: [
      '<img src=\'../images/owl_left.png\'>',
      '<img src=\'../images/owl_right.png\'>'
    ]

    // "singleItem:true" is a shortcut for:
    // items : 1, 
    // itemsDesktop : false,
    // itemsDesktopSmall : false,
    // itemsTablet: false,
    // itemsMobile : false

  });

  $('#owl-decoration-3').owlCarousel({

    navigation: true, // Show next and prev buttons
    slideSpeed: 300,
    paginationSpeed: 400,
    singleItem: true,
    pagination: false,
    navigation: true,
    navigationText: [
      '<img src=\'../images/owl_left.png\'>',
      '<img src=\'../images/owl_right.png\'>'
    ]

    // "singleItem:true" is a shortcut for:
    // items : 1, 
    // itemsDesktop : false,
    // itemsDesktopSmall : false,
    // itemsTablet: false,
    // itemsMobile : false

  });

  $('#owl-decoration-4').owlCarousel({

    navigation: true, // Show next and prev buttons
    slideSpeed: 300,
    paginationSpeed: 400,
    singleItem: true,
    pagination: false,
    navigation: true,
    navigationText: [
      '<img src=\'../images/owl_left.png\'>',
      '<img src=\'../images/owl_right.png\'>'
    ]

    // "singleItem:true" is a shortcut for:
    // items : 1, 
    // itemsDesktop : false,
    // itemsDesktopSmall : false,
    // itemsTablet: false,
    // itemsMobile : false

  });

  $('#owl-decoration-5').owlCarousel({

    navigation: true, // Show next and prev buttons
    slideSpeed: 300,
    paginationSpeed: 400,
    singleItem: true,
    pagination: false,
    navigation: true,
    navigationText: [
      '<img src=\'../images/owl_left.png\'>',
      '<img src=\'../images/owl_right.png\'>'
    ]

    // "singleItem:true" is a shortcut for:
    // items : 1, 
    // itemsDesktop : false,
    // itemsDesktopSmall : false,
    // itemsTablet: false,
    // itemsMobile : false

  });

  $('#owl-decoration-6').owlCarousel({

    navigation: true, // Show next and prev buttons
    slideSpeed: 300,
    paginationSpeed: 400,
    singleItem: true,
    pagination: false,
    navigation: true,
    navigationText: [
      '<img src=\'../images/owl_left.png\'>',
      '<img src=\'../images/owl_right.png\'>'
    ]

    // "singleItem:true" is a shortcut for:
    // items : 1, 
    // itemsDesktop : false,
    // itemsDesktopSmall : false,
    // itemsTablet: false,
    // itemsMobile : false

  });

  $('#owl-sale').owlCarousel({

    navigation: true,
    pagination: true, // Show next and prev buttons
    slideSpeed: 300,
    autoPlay: true,
    paginationSpeed: 400,
    singleItem: true

    // "singleItem:true" is a shortcut for:
    // items : 1, 
    // itemsDesktop : false,
    // itemsDesktopSmall : false,
    // itemsTablet: false,
    // itemsMobile : false

  });

  var sync1 = $('#sync1');
  var sync2 = $('#sync2');

  sync1.owlCarousel({
    singleItem: true,
    slideSpeed: 1000,
    navigation: true,
    pagination: false,
    navigationText: [
      '<img src=\'images/owl_left.png\'>',
      '<img src=\'images/owl_right.png\'>'
    ],
    afterAction: syncPosition,
    responsiveRefreshRate: 200,
  });

  sync2.owlCarousel({
    navigation: false,
    items: 4,
    itemsDesktop: [1199, 10],
    itemsDesktopSmall: [979, 10],
    itemsTablet: [768, 8],
    itemsMobile: [479, 4],
    pagination: false,
    responsiveRefreshRate: 100,
    navigationText: [
      '<img src=\'images/owl_left.png\'>',
      '<img src=\'images/owl_right.png\'>'
    ],
    afterInit: function(el) {
      el.find('.owl-item').eq(0).addClass('synced');
    }
  });

  function syncPosition(el) {
    var current = this.currentItem;
    $('#sync2')
      .find('.owl-item')
      .removeClass('synced')
      .eq(current)
      .addClass('synced')
    if ($('#sync2').data('owlCarousel') !== undefined) {
      center(current)
    }
  }

  $('#sync2').on('click', '.owl-item', function(e) {
    e.preventDefault();
    var number = $(this).data('owlItem');
    sync1.trigger('owl.goTo', number);
  });

  function center(number) {
    var sync2visible = sync2.data('owlCarousel').owl.visibleItems;
    var num = number;
    var found = false;
    for (var i in sync2visible) {
      if (num === sync2visible[i]) {
        var found = true;
      }
    }

    if (found === false) {
      if (num > sync2visible[sync2visible.length - 1]) {
        sync2.trigger('owl.goTo', num - sync2visible.length + 2)
      } else {
        if (num - 1 === -1) {
          num = 0;
        }
        sync2.trigger('owl.goTo', num);
      }
    } else if (num === sync2visible[sync2visible.length - 1]) {
      sync2.trigger('owl.goTo', sync2visible[1])
    } else if (num === sync2visible[0]) {
      sync2.trigger('owl.goTo', num - 1)
    }

  };

});